package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;


public class Main {

    static Set<String> lecturer2WebSet = new HashSet<String>();
    static Set<String> assistGwSet = new HashSet<String>();
    static Set<String> tResumeGatewaySet = new HashSet<String>();
    static Set<String> elearningNewtaskSet = new HashSet<String>();
    static Set<String> diyformGatewaySet = new HashSet<String>();
    static Set<String> elearningArchivesSet = new HashSet<String>();
    static Set<String> smrzSet = new HashSet<String>();
    static Set<String> xxcgSet = new HashSet<String>();
    static Set<String> xjglSet = new HashSet<String>();
    static Set<String> thesisSet = new HashSet<String>();
    static Set<String> zsglSet = new HashSet<String>();


    public static void main(String[] args) {
	    // write your code here
        System.out.println("==============");
        // 讲师组件

        /*File file = new File("E:\\AppFactory_web\\lecturer2-web\\src\\main\\js");
        File targetFile1 = new File("E:\\AppFactory_web\\language\\lecturer2-web.txt");
        getChild(file, targetFile1, lecturer2WebSet);

        File fileAdmin0 = new File("E:\\AppFactory_web\\lecturer2-web\\src\\main\\admin");
        File targetFile0 = new File("E:\\AppFactory_web\\language\\lecturer2-web.txt");
        getChild(fileAdmin0, targetFile0, lecturer2WebSet);*/
        // assist-gw
        /*File fileAdmin = new File("E:\\AppFactory_web\\assist-gw\\src\\main\\webapp\\project\\admin");
        File targetFile2 = new File("E:\\AppFactory_web\\language\\assist-gw.txt");
        getChild(fileAdmin, targetFile2, assistGwSet);
        // 教师履历
        File fileAdmin1 = new File("E:\\AppFactory_web\\t-resume-gateway\\src\\main\\js");
        File targetFile3 = new File("E:\\AppFactory_web\\language\\t-resume-gateway.txt");
        getChild(fileAdmin1, targetFile3, tResumeGatewaySet);*/
        // 任务式学习
        File fileAdmin2 = new File("E:\\AppFactory_web\\elearning-newtask-gateway\\src\\main\\js");
        File targetFile4 = new File("E:\\AppFactory_web\\language\\20171013\\elearning-newtask-gateway-js.txt");
        getChild(fileAdmin2, targetFile4, elearningNewtaskSet);
        // 数据统计
//        File fileAdmin7 = new File("E:\\AppFactory_web\\elearning-newtask-gateway\\src\\main\\js-admin");
//        File targetFile9 = new File("E:\\AppFactory_web\\language\\elearning-newtask-gateway.txt");
//        getChild(fileAdmin7, targetFile9, elearningNewtaskSet);
        // diyform-gateway
//        File fileAdmin11 = new File("E:\\AppFactory_web\\diyform-gateway\\src\\main\\js");
//        File targetFile13 = new File("E:\\AppFactory_web\\language\\diyform-gateway.txt");
//        getChild(fileAdmin11, targetFile13, diyformGatewaySet);
        // 个人档案
//        File fileAdmin3 = new File("E:\\AppFactory_web\\elearning-archives-gateway\\src\\main\\js");
//        File targetFile5 = new File("E:\\AppFactory_web\\language\\elearning-archives-gateway.txt");
//        getChild(fileAdmin3, targetFile5, elearningArchivesSet);
        // 实名认证
//        File fileAdmin4 = new File("E:\\AppFactory_web\\smrz-gateway\\src\\main\\js");
//        File targetFile6 = new File("E:\\AppFactory_web\\language\\smrz-gateway.txt");
//        getChild(fileAdmin4, targetFile6, smrzSet);
        // 学习成果
        /* File fileAdmin6 = new File("E:\\AppFactory_web\\elearning-xxcg-gateway\\src\\main\\js");
        File targetFile8 = new File("E:\\AppFactory_web\\language\\elearning-xxcg-gateway.txt");
        getChild(fileAdmin6, targetFile8, xxcgSet); */
        // 毕业管理
        /*File fileAdmin8 = new File("E:\\AppFactory_web\\elearning-xjgl-gateway\\src\\main\\js-bygl");
        File targetFile10 = new File("E:\\AppFactory_web\\language\\elearning-xjgl-gateway.txt");
        getChild(fileAdmin8, targetFile10, xjglSet);
        // 学籍管理，异动管理
        File fileAdmin9 = new File("E:\\AppFactory_web\\elearning-xjgl-gateway\\src\\main\\js");
        File targetFile11 = new File("E:\\AppFactory_web\\language\\elearning-xjgl-gateway.txt");
        getChild(fileAdmin9, targetFile11, xjglSet);
        // 毕业论文
        File fileAdmin10 = new File("E:\\AppFactory_web\\elearning-thesis-gateway\\src\\main\\js");
        File targetFile12 = new File("E:\\AppFactory_web\\language\\elearning-thesis-gateway.txt");
        getChild(fileAdmin10, targetFile12, thesisSet);
        // 招生管理
        File fileAdmin5 = new File("E:\\AppFactory_web\\zsgl-gateway\\src\\main\\js");
        File targetFile7 = new File("E:\\AppFactory_web\\language\\zsgl-gateway.txt");
        getChild(fileAdmin5, targetFile7, zsglSet);*/
    }

    static void getChild(File file, File targetFile, Set<String> set) {
        File[] files = file.listFiles();
        for (File f : files) {
            if (!f.isDirectory()) {
                String fileName = f.getName();
                if (fileName.endsWith("jsx") || fileName.endsWith("js") || fileName.endsWith("json")) {
                    if ("mock.js".equals(fileName) || fileName.endsWith("bundle.js") || fileName.contains(".min.")) {
                        return;
                    }
                    BufferedReader reader = null;
                    try {
                        reader = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));

                        String tempString = null;
                        Set<String> arr = new HashSet<String>();
                        while ((tempString = reader.readLine()) != null) {
                            String tempStr = tempString.trim();
                            if (tempStr.startsWith("//") || tempStr.startsWith("<!--") || tempStr.startsWith("/*") || tempStr.startsWith("*")
                                    || tempStr.contains("console.") || tempStr.startsWith("{/*") || tempStr.startsWith("alert(")) {
                                //System.out.println(tempString);
                                continue;
                            }
                            Pattern p = Pattern.compile("[\\u4e00-\\u9fa5]+\\S*[\\u4e00-\\u9fa5]+");
                            Matcher m = p.matcher(tempString);

                            while (m.find()) {
                                if (tempString.contains("//") || tempString.contains("/*")) {
                                    System.out.println(tempString + " ||所在项目：" + targetFile.getName());
                                    continue;
                                }
                                String pinyin = getPingYin(m.group());
                                String kv = new StringBuffer("'").append(pinyin).append("': '").append(m.group()).append("',").toString();
                                arr.add(kv);
                            }
                        }
                        writeFile(arr, targetFile, set);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                if (!"node_modules".equals(f.getName()) && !"mock".equals(f.getName()) && !"dist".equals(f.getName()) && !"antjs".equals(f.getName())
                        && !"assets".equals(f.getName()) && !"bg".equals(f.getName())) {
                    getChild(f, targetFile, set);
                }
            }
        }
    }



    static void writeFile(Set<String> arr, File targetFile, Set<String> finalSet) {
        if (!targetFile.exists()) {
            try {
                targetFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileWriter writer = null;
        try {
            writer = new FileWriter(targetFile, true);
            for (String s : arr) {
                if (!finalSet.contains(s)) {
                    finalSet.add(s);
                    writer.append(s + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // 将汉字转换为全拼
    static String getPingYin(String src) {
        if (null == src || "".equals(src)) {
            return "";
        }
        char[] t1 = null;
        t1 = src.toCharArray();
        String[] t2 = null;
        HanyuPinyinOutputFormat t3 = new HanyuPinyinOutputFormat();

        t3.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        t3.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        t3.setVCharType(HanyuPinyinVCharType.WITH_V);
        String t4 = "";
        int t0 = t1.length;
        try {
            for (int i = 0; i < t0; i++) {
                if (java.lang.Character.toString(t1[i]).matches(
                        "[\\u4E00-\\u9FA5]+")) {
                    t2 = PinyinHelper.toHanyuPinyinStringArray(t1[i], t3);
                    t4 += (t2 != null && t2.length > 0) ? t2[0] : "";
                } else
                    t4 += java.lang.Character.toString(t1[i]);
            }
            // System.out.println(t4);
            return t4;
        } catch (BadHanyuPinyinOutputFormatCombination e1) {
            e1.printStackTrace();
        }
        return t4;
    }
}
